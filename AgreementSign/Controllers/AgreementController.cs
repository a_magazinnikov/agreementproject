﻿using System;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using AgreementSign.DAL.Model;
using AgreementSign.DAL.Repositories;
using AgreementSign.Helpers;
using AgreementSign.Models;

namespace AgreementSign.Controllers
{
    public class AgreementController : Controller
    {
        private IAgreementRepository AgreementRepository { get; }

        private ISignRepository SignRepository { get; }

        public AgreementController(IAgreementRepository agreementRepository, ISignRepository signRepository)
        {
            AgreementRepository = agreementRepository;
            SignRepository = signRepository;
        }

        public ActionResult Index()
        {
            var agreement = AgreementRepository.FirstOrDefault();
            var signs = SignRepository.GetAll();
            var useDefualtText = Boolean.Parse(ConfigurationManager.AppSettings["UseDefaultText"]);

            return View(new AgreementViewModel
            {
                Text = agreement?.Text ?? (useDefualtText ? Resources.Agreement.Text : string.Empty),
                Signs = signs.Select(x => new SignViewModel(x)).ToList()
            });
        }

        [HttpPost]
        public ActionResult Sign(AgreementSignViewModel model)
        {
            if (ModelState.IsValid)
            {
                SignRepository.Upsert(new Sign
                {
                    FullName = model.FullName,
                    CreatedDate = DateTime.UtcNow,
                    Signature = model.SignType == SignType.ManualSign? model.Signature : Utils.ImageToByteArray(Utils.ConvertTextToImage(model.UseKeyboardInput.Trim()))
                });
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Edit(AgreementEditViewModel model)
        {
            if (ModelState.IsValid)
            {
                var agreement = AgreementRepository.FirstOrDefault() ?? new Agreement();
                agreement.Text = model.Text;
                AgreementRepository.Upsert(agreement);
            }

            return RedirectToAction("Index");
        }
    }
}
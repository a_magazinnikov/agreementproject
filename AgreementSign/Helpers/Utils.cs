﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Web;

namespace AgreementSign.Helpers
{
    public static class Utils
    {
        public static byte[] ImageToByteArray(Image imageIn)
        {
            var ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
            return ms.ToArray();
        }

        public static Image ByteArrayToImage(byte[] byteArrayIn)
        {
            var ms = new MemoryStream(byteArrayIn);
            var returnImage = Image.FromStream(ms);
            return returnImage;
        }

        public static Bitmap ConvertTextToImage(string txt, string fontname = "Brush Script MT", int fontsize = 75)
        {
            var bmp = new Bitmap(1, 1);
            var bgcolor = Color.White;
            var fcolor = Color.Black;
            var font = new Font(fontname, fontsize, FontStyle.Regular, GraphicsUnit.Pixel);
            var graphics = Graphics.FromImage(bmp);
            var width = 1000;//(int) graphics.MeasureString(txt, font).Width;
            var height = (int) graphics.MeasureString(txt, font).Height;
            height = height == 0 ? 1 : height;
            bmp = new Bitmap(bmp, new Size(width, height));
            graphics = Graphics.FromImage(bmp);
            graphics.FillRectangle(new SolidBrush(bgcolor), 0, 0, bmp.Width, bmp.Height);
            graphics.DrawString(txt, font, new SolidBrush(fcolor), 0, 0);
            graphics.Flush();
            font.Dispose();
            graphics.Dispose();

            return bmp;
        }

        public static string ToImgSource(this byte[] array)
        {
            return array != null ? $"data:image/gif;base64,{Convert.ToBase64String(array)}" : "//";
        }
    }
}
﻿using AgreementSign.DAL;
using AgreementSign.DAL.Repositories;
using LightInject;

namespace AgreementSign
{
    public static class DependencyConfig
    {
        public static void Initialize()
        {
            var container = new ServiceContainer();

            container.Register<AgreementSignContext>();
            container.Register<IAgreementRepository, AgreementRepository>();
            container.Register<ISignRepository, SignRepository>();

            container.RegisterControllers();
            container.EnableMvc();
        }
    }
}
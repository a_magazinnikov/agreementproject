﻿using System;
using AgreementSign.DAL.Model;

namespace AgreementSign.Models
{
    public class SignViewModel
    {
        public SignViewModel(Sign sign)
        {
            Id = sign.Id;
            FullName = sign.FullName;
            CreatedDate = sign.CreatedDate;
            Signature = sign.Signature;
        }

        public long Id { get; set; }

        public string FullName { get; set; }

        public DateTime? CreatedDate { get; set; }

        public byte[] Signature { get; set; }
    }
}
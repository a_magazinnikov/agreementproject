﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using ExpressiveAnnotations.Attributes;

namespace AgreementSign.Models
{
    public class AgreementSignViewModel
    {
        [AllowHtml]
        public string Text { get; set; }

        [Required]
        [Display(Name = "Print Full Name")]
        public string FullName { get; set; }

        [RequiredIf("SignType == 0")]
        [UIHint("SignaturePad")]
        public byte[] Signature { get; set; }

        public SignType SignType { get; set; }

        [RequiredIf("SignType == 1")]
        public string UseKeyboardInput { get; set; }
    }
}
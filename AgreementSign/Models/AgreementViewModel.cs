﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace AgreementSign.Models
{
    public class AgreementViewModel
    {
        [AllowHtml]
        public string Text { get; set; }

        public List<SignViewModel> Signs { get; set; }
    }
}
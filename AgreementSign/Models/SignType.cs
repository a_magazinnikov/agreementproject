﻿namespace AgreementSign.Models
{
    public enum SignType
    {
        ManualSign = 0,
        GeneratedSign = 1
    }
}
﻿using System.Web.Mvc;

namespace AgreementSign.Models
{
    public class AgreementEditViewModel
    {
        [AllowHtml]
        public string Text { get; set; }
    }
}
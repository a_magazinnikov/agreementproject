﻿var pads = {};
var signaturePadWrappers = document.querySelectorAll('.signature-pad');

[].forEach.call(signaturePadWrappers, function (wrapper) {
    var canvas = wrapper.querySelector('canvas');
    var clearButton = wrapper.querySelector('.btn-clear-canvas');
    var hiddenInput = wrapper.querySelector('input[type="hidden"]');

    var signaturePad = new SignaturePad(canvas);

    var resizeCanvas = function () {
        var parentWidth = $(canvas).parent().outerWidth() - 2; // minus border
        canvas.setAttribute("width", parentWidth);
        signaturePad.clear();
    }

    window.addEventListener("resize", resizeCanvas);
    resizeCanvas();
    
    // Read base64 string from hidden input
    var base64str = hiddenInput.value;

    if (base64str) {
        // Draws signature image from data URL
        signaturePad.fromDataURL('data:image/png;base64,' + base64str);
    }

    if (hiddenInput.disabled) {
        signaturePad.off();
        clearButton.classList.add('hidden');
    } else {
        signaturePad.onEnd = function () {
            // Returns signature image as data URL and set it to hidden input
            base64str = signaturePad.toDataURL().split(',')[1];
            hiddenInput.value = base64str;
            $(hiddenInput).trigger('change');
        };

        clearButton.addEventListener('click', function () {
            // Clear the canvas and hidden input
            signaturePad.clear();
            hiddenInput.value = '';
            $(hiddenInput).trigger('change');
        });
    }

    var key = $(wrapper).attr('signature-pad-id');

    pads[key] = signaturePad;
});

function getSignaturePad(wrapper) {
    var key = $(wrapper).attr('signature-pad-id');
    return pads[key];
}

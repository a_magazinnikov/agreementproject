﻿using System.Data.Entity;
using AgreementSign.DAL.Migrations;
using AgreementSign.DAL.Model;

namespace AgreementSign.DAL
{
    public class AgreementSignContext : DbContext
    {
        public AgreementSignContext() : base("name=AgreementSignContext")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<AgreementSignContext, Configuration>());
        }

        public IDbSet<Agreement> Agreements { get; set; }

        public IDbSet<Sign> Signs { get; set; }
    }
}
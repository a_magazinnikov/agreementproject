﻿using AgreementSign.DAL.Model;

namespace AgreementSign.DAL.Repositories
{
    public class SignRepository : Repository<Sign>, ISignRepository
    {
        public SignRepository(AgreementSignContext context) : base(context)
        {
        }
    }
}
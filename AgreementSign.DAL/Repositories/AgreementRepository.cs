﻿using AgreementSign.DAL.Model;

namespace AgreementSign.DAL.Repositories
{
    public class AgreementRepository : Repository<Agreement>, IAgreementRepository
    {
        public AgreementRepository(AgreementSignContext context) : base(context)
        {
        }
    }
}
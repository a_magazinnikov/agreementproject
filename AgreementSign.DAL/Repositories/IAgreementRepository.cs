﻿using AgreementSign.DAL.Model;

namespace AgreementSign.DAL.Repositories
{
    public interface IAgreementRepository : IRepository<Agreement>
    {
    }
}
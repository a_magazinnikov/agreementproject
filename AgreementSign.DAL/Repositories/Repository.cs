﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using AgreementSign.DAL.Model;

namespace AgreementSign.DAL.Repositories
{
    public class Repository<T> : IRepository<T> where T : BaseEntity
    {
        protected AgreementSignContext DbContext { get; set; }

        protected Repository(AgreementSignContext context)
        {
            DbContext = context;
        }

        public T FirstOrDefault()
        {
            return DbContext.Set<T>().FirstOrDefault();
        }

        public virtual T Get(long id)
        {
            return DbContext.Set<T>().Find(id);
        }

        public virtual List<T> GetAll()
        {
            return DbContext.Set<T>().ToList();
        }

        public virtual T Upsert(T obj)
        {
            if (obj.IsNew)
            {
                DbContext.Set<T>().Add(obj);
            }
            else
            {
                DbContext.Entry(obj).State = EntityState.Modified;
            }

            DbContext.SaveChanges();

            return obj;
        }

        public virtual void Delete(long id)
        {
            var item = DbContext.Set<T>().Find(id);
            if (item != null)
            {
                DbContext.Set<T>().Remove(item);
                DbContext.SaveChanges();
            }
        }
    }
}
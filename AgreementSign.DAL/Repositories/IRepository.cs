﻿using System.Collections.Generic;
using AgreementSign.DAL.Model;

namespace AgreementSign.DAL.Repositories
{
    /// <summary>
    /// Generic repository base interface. 
    /// Provides basic CRUD (Create, Read, Update, Delete) and a couple more methods.
    /// </summary>
    public interface IRepository<T> where T : BaseEntity
    {
        /// <summary>
        /// Reads an individual item.
        /// </summary>
        /// <param name="id">The business object's id.</param>
        /// <returns></returns>
        T FirstOrDefault();

        /// <summary>
        /// Reads an individual item.
        /// </summary>
        /// <param name="id">The business object's id.</param>
        /// <returns></returns>
        T Get(long id);

        /// <summary>
        /// Get all items.
        /// </summary>
        /// <returns></returns>
        List<T> GetAll();

        /// <summary>
        /// Inserts/Update a new item.
        /// </summary>
        /// <param name="t">The business object. </param>
        T Upsert(T t);

        /// <summary>
        /// Deletes an item.
        /// </summary>
        /// <param name="id">The business object's id</param>
        void Delete(long id);
    }
}

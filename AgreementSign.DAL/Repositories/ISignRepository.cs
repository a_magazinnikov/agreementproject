﻿using AgreementSign.DAL.Model;

namespace AgreementSign.DAL.Repositories
{
    public interface ISignRepository : IRepository<Sign>
    {
    }
}
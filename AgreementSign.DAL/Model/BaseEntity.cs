﻿namespace AgreementSign.DAL.Model
{
    public abstract class BaseEntity
    {
        public long Id { get; set; }

        public bool IsNew => Id == 0;
    }
}
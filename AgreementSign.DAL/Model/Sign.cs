﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace AgreementSign.DAL.Model
{
    public class Sign : BaseEntity
    {
        public string FullName { get; set; }

        public DateTime CreatedDate { get; set; }

        [Column(TypeName = "varbinary(MAX)")]
        public byte[] Signature { get; set; }
    }
}
using System.Data.Entity.Migrations;

namespace AgreementSign.DAL.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<AgreementSignContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(AgreementSignContext context)
        {
        }
    }
}